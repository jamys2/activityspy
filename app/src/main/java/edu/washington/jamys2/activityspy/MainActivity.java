package edu.washington.jamys2.activityspy;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private static final String FIRE = " event fired";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            Log.i(TAG, savedInstanceState.toString());
        } else {
            Log.i(TAG, "onCreate" + FIRE);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart" + FIRE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart" + FIRE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume" + FIRE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause" + FIRE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop" + FIRE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "We're going down, Captain!");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
